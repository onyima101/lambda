"""START-Function:"""

import boto3

def lambda_handler(event, context):
    ec2 = boto3.resource('ec2')
    
    filters = [{
        'Name': 'tag:DailyStartStop',
        'Values': ['Yes', 'True']
    }]

    instances = ec2.instances.filter(Filters=filters)
    matchingIds = [instance.id for instance in instances]
    
    if len(matchingIds) > 0:
        startingUp = ec2.instances.filter(InstanceIds=matchingIds).start()
        print('started ' + str(len(matchingIds)) + ' instances')
    else:
        print("no instances matched")
